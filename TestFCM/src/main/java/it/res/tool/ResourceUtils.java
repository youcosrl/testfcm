package it.res.tool;

import it.youco.fcm.TestFCM;
import net.sf.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class ResourceUtils {

	/**
	 * Reads given resource file as a string.
	 *
	 * @param fileName path to the resource file
	 * @return the file's contents
	 */
	public static String getResourceFileAsString(String fileName) {
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		try (InputStream is = classLoader.getResourceAsStream(fileName)) {
			if (is == null) return null;
			try (InputStreamReader isr = new InputStreamReader(is);
					BufferedReader reader = new BufferedReader(isr)) {
				return reader.lines().collect(Collectors.joining(System.lineSeparator()));
			}
		} catch (IOException e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * Reads given resource file as an InputStream.
	 *
	 * @param fileName path to the resource file
	 * @return the file's InputStream
	 */
	public static InputStream getResourceFileAsInputStream(String fileName) {
		return ResourceUtils.class.getClassLoader().getResourceAsStream(fileName);
	}

	/**
	 * Reads given resource file as a parsed JSON Object.
	 *
	 * @param fileName path to the resource file
	 * @return a parsed JSON Object
	 */
	public static JSONObject getResourceFileAsJsonObject(String fileName) {
		return JSONObject.fromObject(getResourceFileAsString(fileName));
	}
}
