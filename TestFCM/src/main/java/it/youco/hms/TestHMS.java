package it.youco.hms;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Date ;
import java.util.stream.Collectors;

import it.res.tool.ResourceUtils;
import net.sf.json.JSONObject;

import com.huawei.push.exception.HuaweiMesssagingException;
import com.huawei.push.message.AndroidConfig;
import com.huawei.push.message.Message;
import com.huawei.push.messaging.HuaweiApp;
import com.huawei.push.messaging.HuaweiMessaging;
import com.huawei.push.model.Urgency;
import com.huawei.push.reponse.SendResponse;
import com.huawei.push.util.InitAppUtils;


public class TestHMS {

	public static void main(String[] args) {
		
		inviaNotificaEshCare();
	}

	
	public static void inviaNotificaEshCare() {
		try {
			HuaweiApp app = InitAppUtils.initializeApp();
	        HuaweiMessaging huaweiMessaging = HuaweiMessaging.getInstance(app);

	        
	        AndroidConfig androidConfig = AndroidConfig.builder()
	        		.setCollapseKey(-1)
	                .setUrgency(Urgency.NORMAL.getValue())
	                .setTtl("10000s")
	                //.setBiTag("the_sample_bi_tag_for_receipt_service")
	                .build();

	        String token = "IQAAAACy0mCbAACQYVkL7mZazlVLir3VONYTaAcCQIpfnJdFg46KiNyV9g0H5useUn0SOnw2F1JMotwOBxqQgjyk5T-FaFqDxVk1artIBCSPoFEifw";

			JSONObject obj = new JSONObject();
	    	obj.put("id", String.valueOf(System.currentTimeMillis()));
	    	obj.put("title", "TEST TITOLO");
	    	obj.put("sender", "TEST MITTENTE");
	    	obj.put("msg", "Messaggio da visualizzare");
	    	obj.put("date", String.valueOf(System.currentTimeMillis()));

	        
	        Message message = Message.builder()
	                .setData(obj.toString())
	                .setAndroidConfig(androidConfig)
	                .addToken(token)
	                .build();
	        
	        
	        SendResponse response = huaweiMessaging.sendMessage(message);
			
			System.out.println("Successfully sent message: " + response.getCode() + " - "+ response.getMsg());
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
//	public static void testEshCare() {
//		try {
//			FileInputStream serviceAccount = new FileInputStream("serviceAccount/esh-care-firebase-adminsdk-jgkee-e3034a5c87.json");
//
//			FirebaseOptions options = new FirebaseOptions.Builder()
//			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//			  .setDatabaseUrl("https://esh-care.firebaseio.com")
//			  .build();
//
//			FirebaseApp.initializeApp(options);
//			
//			/*
//			 * Valorizzare il registrationToken con il valore che si ottiene dal client quando si effettua la registrazione al server fcm
//			 */
//			//String registrationToken = "c1a9yJ-GYz0:APA91bHFvkQaTS5kIWpxF0_nMqZO3-Rct7tN8u0witoN5HGi2ha8QUJfEUrad-MOtpOmbkh9bQ54DMlj-m5WzxmB-ZDuzN95AOfbkvHIpZ2j1VLk6ZywQB3deM3PcMfMPTldX4rwMVL-";
//
//			String registrationToken = "ftxZYSaZTAWVuy71ypFmKv:APA91bHF_1ueGexzvUUHhdtDTXb-bmL4gDpYnmyAZvVdQ6nS5uZgWwhFy_CY2FXmDg6019K9WlQSN-FYrAoPb7ReHwYLxiXd4ghogU5ZD7Ye1XilZODLcjP1d6HFy_crhwtAToDfZENV";
//
//
//			boolean perAndroid = false; // scelgo false per IOS
//			
//			Message message = null;
//			
//			if (perAndroid ) {
//			
//			/* ANDROID */
//			 message = Message.builder()
//					.putData("id", String.valueOf(System.currentTimeMillis()))
//				    .putData("title", "TEST TITOLO 4 ")
//				    .putData("sender", "TEST MITTENTE 4 ")
//				    .putData("msg", "Messaggio da visualizzare 4")
//				    .putData("date", String.valueOf(System.currentTimeMillis()))
//				    .setToken(registrationToken)
//				    .build();
//			
//			}
//			else {
//			/*IOS */
//			
//				
//			String sTitolo = "Titolo";
//			String sMsg = "messaggio ripetitivo";
//			
//			JSONObject jsonNotification = new JSONObject();
//			jsonNotification.put("id", 100151); //String.valueOf(System.currentTimeMillis() ));
//			jsonNotification.put("title",  sTitolo );
//			jsonNotification.put("sender", "test mittente 4" );
//			jsonNotification.put("date", ""+String.valueOf(System.currentTimeMillis() ));
//			jsonNotification.put("msg", sMsg);
//
//		
//			
//			
//			//CREO IL MESSAGGIO IOS
//			message = Message.builder()
//					
//					.setToken( registrationToken ) 					        					        					       					        					        					  					    
//					.setApnsConfig(ApnsConfig.builder()
//							.putHeader("apns-priority", "10")
//							.putHeader("apns_push_type", "background")
//							.setAps(Aps.builder()
//									.setSound(CriticalSound.builder() // All fields
//											.setCritical(true)
//											.setName("default")
//											.setVolume(1)
//											.build()) 	
//									.setContentAvailable(true)
//									.putCustomData("eshbody", jsonNotification.toString() )
//									.setContentAvailable(true)																	
//									.setAlert(ApsAlert.builder()
//											.setTitle(sTitolo )
//											.setBody( sMsg )
//											.build())
////									.setBadge(42)
//									.build())
//							.build())
//					.build();
//			}
//			
//			String response = FirebaseMessaging.getInstance().send(message);
//			
//			System.out.println("Successfully sent message: " + response);
//			
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}



}