package it.youco.workforce.tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;

import it.res.tool.DBHandler;
import it.res.tool.ResourceUtils;
import it.youco.YouTruckServlet;
import it.youco.fcm.InfoLog;
import net.sf.json.JSONObject;

public class TestFCMClientUnico {

	public static final String WORKFORCE_FIREBASE_APP = "[DEFAULT]";
	/*public static final String WORKFORCE_FIREBASE_APP = "WORKFORCEFCM";*/
	public static String NOTIFICA_MESSAGE_KEY_STR_PAGE = "page";
	public static String TIPO_NOTIFICA_PAGE_YOUTALKIE = "/vocal-list";

	final static String FIREBASE_ACCOUNT_FILE_PATH = "serviceAccount/youworkforce-firebase-adminsdk-72smp-29f94feb0e.json";
	final static String FIREBASE_ENDPOINT = "https://esh-care.firebaseio.com";
	final static String FCM_TOKEN = "fBhYLwq-Ak-tgfcNdWKs__:APA91bE2bmpWurHeZVBwUU9aT47p5ZbKg8KIcikOxlMHyOblVn-2t__AW0MZhqbO58PS7Z-KSU9VHb0iCgHVnRFtyADYd_gKKEYbw8fYDiW0JW694xqy_MDAMO9nBDLvY0CEH_iKiz6C";

	final static String PIN = "4AD781D8-7A33-45A8-8FBA-0A0A848A04C0";
	final static Long EXPIRATION_SECONDS = 300l;
	final static boolean SILENTE = false;
	
	
	final static String PUSH_TYPE = "p2t";
	/*
	 * p2t = PUSH TO TALK, 
	 * text = you message text
	 * pending = INVIO_MESSAGGI_PENDING,
	 * mandown-on = Attivazione Silenziosa Mandown
	 * mandown-off = Disattivazione Silenziosa Mandown
	 */
	
	public static void main(String[] args) {
		initializeFirebase();

		if (PUSH_TYPE=="text") {
			JSONObject payload = ResourceUtils.getResourceFileAsJsonObject("test-payloads/youmessage.json");
			generateYouMessageMockMessageId(payload);
			inviaNotificaYouMessageFcmClientUnico(new DBHandler(), PIN, payload, FCM_TOKEN, EXPIRATION_SECONDS, SILENTE);
		}

		else if(PUSH_TYPE == "p2t") {
			JSONObject payload = ResourceUtils.getResourceFileAsJsonObject("test-payloads/youtalkie.json");
			generateYouTalkieMockMessageId(payload);
			inviaNotificaYouMessageFcmClientUnico(new DBHandler(), PIN, payload, FCM_TOKEN, EXPIRATION_SECONDS, SILENTE);
		}
		
		else if(PUSH_TYPE == "pending") {
			JSONObject payload = ResourceUtils.getResourceFileAsJsonObject("test-payloads/pending.json");
			inviaPushPendingClientUnico(new DBHandler(), PIN, payload, FCM_TOKEN, EXPIRATION_SECONDS);
		}

		else if(PUSH_TYPE == "mandown-on") {
			JSONObject payload = ResourceUtils.getResourceFileAsJsonObject("test-payloads/mandown-on.json");
			generateYouTalkieMockMessageId(payload);
			inviaNotificaYouMessageFcmClientUnico(new DBHandler(), PIN, payload, FCM_TOKEN, EXPIRATION_SECONDS, SILENTE);
		}

		else if(PUSH_TYPE == "mandown-off") {
			JSONObject payload = ResourceUtils.getResourceFileAsJsonObject("test-payloads/mandown-off.json");
			generateYouTalkieMockMessageId(payload);
			inviaNotificaYouMessageFcmClientUnico(new DBHandler(), PIN, payload, FCM_TOKEN, EXPIRATION_SECONDS, SILENTE);
		}

	}

	private static void generateYouMessageMockMessageId(JSONObject payload) {
		payload.put("msg_id", Long.toString(  new Date().getTime() ));
	}

	private static void generateYouTalkieMockMessageId(JSONObject payload) {
		payload.put("msg_id", Long.toString(  new Date().getTime() ));
	}

	public static void initializeFirebase() {
		try {
			InputStream serviceAccount = ResourceUtils.getResourceFileAsInputStream(FIREBASE_ACCOUNT_FILE_PATH);
			FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl(FIREBASE_ENDPOINT).build();
			FirebaseApp.initializeApp(options);
		} catch (IOException e){
			throw new RuntimeException(e);
		}
	}

	public static boolean inviaNotificaAndroidFcm(DBHandler dbconn, String sPin, JSONObject jsDati, String sRegistrationId, long lDurataMassimaVitaDelMessaggio) {
		boolean bEsito = false;

		try {
			YouTruckServlet.scriviLogWorkForceFCM("inviaNotificaAndroidFcm " + sPin);
			FirebaseApp firebaseApp = null;

			List<FirebaseApp> appsList = FirebaseApp.getApps();
			for(FirebaseApp app : appsList){
				if(app.getName().equalsIgnoreCase(WORKFORCE_FIREBASE_APP)) {
					YouTruckServlet.scriviLogWorkForceFCM("FCM EXIST:" + app.getName());
					firebaseApp = app;
					break;
				}
			}

			if(firebaseApp != null) {

				jsDati.put(NOTIFICA_MESSAGE_KEY_STR_PAGE, TIPO_NOTIFICA_PAGE_YOUTALKIE);
				boolean destIos = sPin.indexOf('-') > 0;
				YouTruckServlet.scriviLogWorkForceFCM("PIN: " + sPin + " IOS: " + destIos + " DATI: " + jsDati);

				if(sRegistrationId != null && sRegistrationId.length() > 0){
					Map<String, String> map = new HashMap<>();
					Set<String> keySet = jsDati.keySet();

					String title = "";
					String body = "";

					//TODO
					/*
					 * Seva 15/09/2020
					 * Le label sono cablate nel codice... andrebbero spostate per gestire la lingua
					 */
					for(String key: keySet) {

						String val = jsDati.get(key).toString();
						if (key.equalsIgnoreCase("data")) {
							body = "Audio of " + val;
						}
						if (key.equalsIgnoreCase("nome_mittente")) title = "From " + val;
						map.put(key, val);
					}

					/*
					 * Si tratta di un destinatario ios se nel pin ci sono dei trattini
					 */
					Message message = null;
					if (destIos) {
						message = Message.builder()
								.putAllData(map)
								.setNotification(new Notification(title, body))
								.setToken(sRegistrationId)
								.setAndroidConfig(AndroidConfig.builder()
										.setTtl(lDurataMassimaVitaDelMessaggio*1000)
										.build())
								.setApnsConfig(ApnsConfig.builder()
										.putHeader("apns-priority", "10")
										.putHeader("apns_push_type", "background")
										.setAps(Aps.builder()
												.setSound(CriticalSound.builder()
														.setCritical(true)
														.setName("ricezione.caf")
														.setVolume(1)
														.build())
												.putCustomData("notificationbody", jsDati.toString())
												.setContentAvailable(true)
												.setBadge(1)
												.build())
										.build())
								.build();

					} else {
						message = Message.builder()
								.putAllData(map)
								.setNotification(new Notification(title, body))
								.setToken(sRegistrationId)
								.setAndroidConfig(AndroidConfig.builder()
										.setTtl(lDurataMassimaVitaDelMessaggio*1000)
										.build())
								.setApnsConfig(ApnsConfig.builder()
										.putHeader("apns-priority", "10")
										.putHeader("apns_push_type", "background")
										.setAps(Aps.builder()
												.setSound(CriticalSound.builder()
														.setCritical(true)
														.setName("default")
														.setVolume(1)
														.build())
												.putCustomData("notificationbody", jsDati.toString())
												.setContentAvailable(true)
												.setBadge(1)
												.build())
										.build())
								.build();
					}

					String sEsito = "";
					FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance(firebaseApp);
					if(firebaseMessaging != null) {
						sEsito = firebaseMessaging.send(message);
						YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmNew PIN: " + sPin + " - ESITO: " + sEsito);

						bEsito = true;

					} else {
						YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmNew PIN: " + sPin + " - FirebaseMessaging non valido!");
					}

				}else{
					YouTruckServlet.scriviLogWorkForceFCM("PIN: " + sPin + " - RegistrationId vuoto");
				}

			} else {
				YouTruckServlet.scriviLogWorkForceFCM("FIREBASE APP NON INIZIALIZZATA!");
			}

		}catch(Exception e){
			YouTruckServlet.scriviLogWorkForceFCM("ERRORE: TGestioneWorkForce.inviaNotificaAndroidFcm - " + e.getMessage());
			System.out.println("ERRORE: TGestioneWorkForce.inviaNotificaAndroidFcm - " + e.getMessage());
			e.printStackTrace();
		}

		return bEsito;
	}

	private static FirebaseApp getFirebaseApp(){
		List<FirebaseApp> appsList = FirebaseApp.getApps();
		for(FirebaseApp app : appsList){
			if(app.getName().equalsIgnoreCase(WORKFORCE_FIREBASE_APP)) {
				YouTruckServlet.scriviLogWorkForceFCM("FCM EXIST:" + app.getName());
				return app;
			}
		}
		return null;
	}

	public static boolean inviaNotificaYouMessageFcmClientUnico(DBHandler dbconn, String sPin, JSONObject jsDati, String sRegistrationId, long lDurataMassimaVitaDelMessaggio, boolean silente) {
		boolean bEsito = false;
		boolean isIOS = sPin.indexOf('-') > 0;

		try {
			FirebaseApp firebaseApp = getFirebaseApp();
			if (firebaseApp == null) {
				YouTruckServlet.scriviLogWorkForceFCM("FIREBASE APP NON INIZIALIZZATA!");
				return false;
			}

			YouTruckServlet.scriviLogWorkForceFCM("****************************************************************************************");
			YouTruckServlet.scriviLogWorkForceFCM("PIN: " + sPin + " DATI: " + jsDati);

			if(sRegistrationId == null || sRegistrationId.length() == 0) {
				YouTruckServlet.scriviLogWorkForceFCM("ClientUnico PIN: " + sPin + " - RegistrationId vuoto");
				return false;
			}

			Map<String, String> map = new HashMap<>();
			Set<String> keySet = jsDati.keySet();
			String title = "";
			String body = "";

			for(String key: keySet) {
				String val = jsDati.get(key).toString();
				if (key.equalsIgnoreCase("msg_testo")) body = val;
				if (key.equalsIgnoreCase("msg_mittente")) title = val;
				map.put(key, val);
			}

			Message message;
			YouTruckServlet.scriviLogWorkForceFCM("silente: " + silente);
			if (isIOS && silente){
				YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmClientUnico iOS Silente PIN: " + sPin);
				message = getMessageSilente(jsDati, sRegistrationId, lDurataMassimaVitaDelMessaggio, map, title, body);
			} else if (isIOS){
				YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmClientUnico iOS PIN: " + sPin);
				message = getIOSMessageVisible(jsDati, sRegistrationId, lDurataMassimaVitaDelMessaggio, map, title, body);
			} else {
				YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmClientUnico Android PIN: " + sPin);
				message = getAndroidVisibleMessage(jsDati, sRegistrationId, lDurataMassimaVitaDelMessaggio, map, title, body);
			}

			String sEsito = "";
			FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance(firebaseApp);
			if(firebaseMessaging == null) {
				YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmNewClientUnico PIN: " + sPin + " - FirebaseMessaging non valido!");
				return false;
			}

			sEsito = firebaseMessaging.send(message);
			YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmNewClientUnico PIN: " + sPin + " - ESITO: " + sEsito);

			bEsito = true;

		} catch(Exception e){
			YouTruckServlet.scriviLogWorkForceFCM("ERRORE: TGestioneWorkForce.inviaNotificaAndroidFcmClientUnico - " + e.getMessage());
			System.out.println("ERRORE: TGestioneWorkForce.inviaNotificaAndroidFcmClientUnico - " + e.getMessage());
			e.printStackTrace();
		}

		return bEsito;
	}

	private static Message getIOSMessageVisible(JSONObject jsDati, String sRegistrationId, long lDurataMassimaVitaDelMessaggio,
			Map<String, String> map, String title, String body) {
		return Message.builder()
				.putAllData(map)
				.setNotification(new Notification(title, body))
				.setToken(sRegistrationId)
				.setAndroidConfig(AndroidConfig.builder()
						.setTtl(lDurataMassimaVitaDelMessaggio *1000)
						.build())
				.setApnsConfig(ApnsConfig.builder()
						.putHeader("apns-priority", "10")
						.putHeader("apns_push_type", "background")
						.setAps(Aps.builder()
								.setSound(CriticalSound.builder()
										.setCritical(true)
										.setName("confermapresenza.caf")
										.setVolume(1)
										.build())
								.putCustomData("notificationbody", jsDati.toString())
								.setContentAvailable(true)
								//	.setBadge(42)
								.build())
						.build())
				.build();
	}

	private static Message getAndroidVisibleMessage(JSONObject jsDati, String sRegistrationId, long lDurataMassimaVitaDelMessaggio,
			Map<String, String> map, String title, String body) {
		return Message.builder()
				.putAllData(map)
				.setNotification(new Notification(title, body))
				.setToken(sRegistrationId)
				.setAndroidConfig(AndroidConfig.builder()
						.setTtl(lDurataMassimaVitaDelMessaggio*1000)
						.setNotification(AndroidNotification.builder()
								.setTitle(title)
								.setBody(body)
								.setClickAction("FLUTTER_NOTIFICATION_CLICK")
								.build())
						.build())
				.setApnsConfig(ApnsConfig.builder()
						.putHeader("apns-priority", "10")
						.putHeader("apns_push_type", "background")
						.setAps(Aps.builder()
								.setSound(CriticalSound.builder()
										.setCritical(true)
										.setName("default")
										.setVolume(1)
										.build())
								.putAllCustomData(jsDati)
								.setContentAvailable(true)
								.setBadge(1)
								.build())
						.build())
				.build();
	}

	private static Message getMessageSilente(JSONObject jsDati, String sRegistrationId, long lDurataMassimaVitaDelMessaggio,
			Map<String, String> map, String title, String body) {
		return Message.builder()
				.putAllData(map)
				.setNotification(new Notification(title, body))
				.setToken(sRegistrationId)
				.setAndroidConfig(AndroidConfig.builder()
						.setTtl(lDurataMassimaVitaDelMessaggio *1000)
						.build())
				.setApnsConfig(ApnsConfig.builder()
						.putHeader("apns-priority", "10")
						.putHeader("apns_push_type", "background")
						.setAps(Aps.builder()
								.putCustomData("notificationbody", jsDati.toString())
								.setContentAvailable(true)
								//	.setBadge(42)
								.build())
						.build())
				.build();
	}

	private static <T> T recuperaParametro(String sKeyParametro, Class<T> type, JSONObject objConfYouTruck, JSONObject objConfYouMessage, JSONObject objConfYouTalkie, boolean mantieni, InfoLog infoLog) {
		T ret = null;

		try {
			if(objConfYouTruck != null && objConfYouTruck.containsKey(sKeyParametro)) {
				ret = (T) objConfYouTruck.get(sKeyParametro);

			} else if(objConfYouTalkie != null && objConfYouTalkie.containsKey(sKeyParametro)) {
				ret = (T) objConfYouTalkie.get(sKeyParametro);

			} else if(objConfYouMessage != null && objConfYouMessage.containsKey(sKeyParametro)) {
				ret = (T) objConfYouMessage.get(sKeyParametro);
			}

			//Rimuovo il parametro comune dai vari JSON, se esiste
			if (!mantieni) {
				if(objConfYouTruck != null) {

					if (objConfYouTruck.has(sKeyParametro )) objConfYouTruck.remove(sKeyParametro);
				}

				if(objConfYouTalkie != null) {
					if (objConfYouTalkie.has(sKeyParametro )) objConfYouTalkie.remove(sKeyParametro);
				}

				if(objConfYouMessage != null) {
					if (objConfYouMessage.has(sKeyParametro )) objConfYouMessage.remove(sKeyParametro);
				}
			}

		} catch(Exception e){
			YouTruckServlet.scriviLog("ERRORE: TGestioneWorkForce.recuperaParametro - " + e.getMessage(), infoLog);
			System.out.println("ERRORE: TGestioneWorkForce.recuperaParametro - " + e.getMessage());
			e.printStackTrace();
		}

		return ret;
	}

	
	public static boolean inviaPushPendingClientUnico(DBHandler dbconn, String sPin, JSONObject jsDati, String sRegistrationId, long lDurataMassimaVitaDelMessaggio) {
		boolean bEsito = false;
		boolean isIOS = sPin.indexOf('-') > 0;

		try {
			FirebaseApp firebaseApp = getFirebaseApp();
			if (firebaseApp == null) {
				YouTruckServlet.scriviLogWorkForceFCM("FIREBASE APP NON INIZIALIZZATA!");
				return false;
			}

			YouTruckServlet.scriviLogWorkForceFCM("****************************************************************************************");
			YouTruckServlet.scriviLogWorkForceFCM("PIN: " + sPin + " DATI: " + jsDati);

			if(sRegistrationId == null || sRegistrationId.length() == 0) {
				YouTruckServlet.scriviLogWorkForceFCM("ClientUnico PIN: " + sPin + " - RegistrationId vuoto");
				return false;
			}

			Map<String, String> map = new HashMap<>();

			Set<String> keySet = jsDati.keySet();
			for(String key: keySet) {
				String val = jsDati.get(key).toString();
				map.put(key, val);
			}
			
			MulticastMessage message;
			
			List<String> registrationTokens = new Vector<String>(); 
			registrationTokens.add(sRegistrationId);
			
			if (isIOS){
				YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmClientUnico iOS PIN: " + sPin);
				message = getIOSMessagePeriodico(jsDati, registrationTokens, lDurataMassimaVitaDelMessaggio, map);
			} else {
				YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmClientUnico Android PIN: " + sPin);
				message = getAndroidMessagePeriodico(jsDati, registrationTokens, lDurataMassimaVitaDelMessaggio, map);
			}

			String sEsito = "";
			FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance(firebaseApp);
			YouTruckServlet.scriviLogWorkForceFCM("TGestioneWorkForce inviaNotificaAndroidFcmNewClientUnico PIN: " + sPin + " - ESITO: " + sEsito);

			BatchResponse response = null;
			if(firebaseMessaging != null) {
				response = firebaseMessaging.sendMulticast(message);							
				YouTruckServlet.scriviLogWorkForceFCM("ThreadInvioNotificaPeriodicaWorkForceFCM : success: " + response.getSuccessCount() + " failure:" + response.getFailureCount());
				
			} else {
				YouTruckServlet.scriviLogWorkForceFCM("ThreadInvioNotificaPeriodicaWorkForceFCM - FirebaseMessaging non valido!");
			}			
			
			
			bEsito = true;

		} catch(Exception e){
			YouTruckServlet.scriviLogWorkForceFCM("ERRORE: TGestioneWorkForce.inviaNotificaAndroidFcmClientUnico - " + e.getMessage());
			System.out.println("ERRORE: TGestioneWorkForce.inviaNotificaAndroidFcmClientUnico - " + e.getMessage());
			e.printStackTrace();
		}

		return bEsito;
	}
	
	
	private static MulticastMessage getIOSMessagePeriodico(JSONObject jsDati, List<String> registrationTokens, long lDurataMassimaVitaDelMessaggio,
			Map<String, String> map) {
		MulticastMessage message = MulticastMessage.builder()
				.putAllData(map)
				.setNotification(new Notification("", ""))
				.addAllTokens(registrationTokens)
			    .setAndroidConfig(AndroidConfig.builder()
			    		.setTtl(lDurataMassimaVitaDelMessaggio*1000)
			    		.build())
			    .setApnsConfig(ApnsConfig.builder()
						.putHeader("apns-priority", "10")
						.putHeader("apns_push_type", "background")
						.setAps(Aps.builder()
								.putCustomData("notificationbody", jsDati.toString())
								.setContentAvailable(true)	
								.build())
						.build())
				.build();
		
		return message;
	}	
	
	private static MulticastMessage getAndroidMessagePeriodico(JSONObject jsDati, List<String> registrationTokens, long lDurataMassimaVitaDelMessaggio,
			Map<String, String> map) {
		MulticastMessage message = MulticastMessage.builder()
				.putAllData(map)
//				.setNotification(new Notification("", ""))
				.addAllTokens(registrationTokens)
			    .setAndroidConfig(AndroidConfig.builder()
			    		.setTtl(lDurataMassimaVitaDelMessaggio*1000)
						.setNotification(AndroidNotification.builder().build())
			    		.build())
				.build();
		
		return message;
	}		
}
