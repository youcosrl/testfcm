package it.youco;

import it.youco.fcm.InfoLog;

/**
 * Mock Class */
public class YouTruckServlet {

	public static void scriviLog(String message){
		System.out.println(message);
	}

	public static void scriviLogWorkForceFCM(String message){
		System.out.println(message);
	}

	public static void scriviLog(String message, InfoLog infoLog) {
		scriviLog(message);
	}
}
