package it.youco.fcm;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Date ;
import java.util.stream.Collectors;

import it.res.tool.ResourceUtils;
import net.sf.json.JSONObject;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.ApsAlert;
import com.google.firebase.messaging.CriticalSound;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;


public class TestFCM {

	public static void main(String[] args) {
		
		//testYouWorkForPharma();
		//testEshCare();
		//testYouEMM();

		
		
		//inviaNotificaYouMessage();		
		
		//INVIO_MESSAGGI_PENDING per l'invio dei messaggi falliti
		//SYNCRO_MESSAGGI per i messaggi non ricevuti
		//INVIO_LOG per i log
		//inviaComandoYouMessage("INVIO_MESSAGGI_PENDING") ;
		//inviaComandoYouMessage("SYNCRO_MESSAGGI") ;

		inviaNotificaYouMessage();
		//inviaNotificaYouTalkie();
	}

	/*
	public static void testEshCare() {
		try {
			FileInputStream serviceAccount = new FileInputStream("serviceAccount\\esh-care-firebase-adminsdk-jgkee-e3034a5c87.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://esh-care.firebaseio.com")
			  .build();

			FirebaseApp.initializeApp(options);
			
			
			// Valorizzare il registrationToken con il valore che si ottiene dal client quando si effettua la registrazione al server fcm
			
			String registrationToken = "fb1C2IniiPg:APA91bHKKnqFqO0jeGdPxwBxn-42uum6vMk3B3lPfMg8D-UtgTq6EaJmE8_Pb6ScE_OUNmZaPdLOHGxHQdgLEhmVwqYsR6YtQCIUkr-xZdW5kziJQjtmbwh4ebgC0MWWPyawAj_OKC0t";
			
			Message message = Message.builder()
					.putData("id", String.valueOf(System.currentTimeMillis()))
					.putData("from", "MITTENTE")
					.putData("title", "TEST TITOLO")
				    .putData("msg", "Messaggio da visualizzare")
				    .putData("date", String.valueOf(System.currentTimeMillis()))
				    .setToken(registrationToken)
				    .build();
			
			String response = FirebaseMessaging.getInstance().send(message);
			
			System.out.println("Successfully sent message: " + response);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	
//	public static void testEshCare() {
//		try {
//			FileInputStream serviceAccount = new FileInputStream("serviceAccount/esh-care-firebase-adminsdk-jgkee-e3034a5c87.json");
//
//			FirebaseOptions options = new FirebaseOptions.Builder()
//			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//			  .setDatabaseUrl("https://esh-care.firebaseio.com")
//			  .build();
//
//			FirebaseApp.initializeApp(options);
//			
//			/*
//			 * Valorizzare il registrationToken con il valore che si ottiene dal client quando si effettua la registrazione al server fcm
//			 */
//			//String registrationToken = "c1a9yJ-GYz0:APA91bHFvkQaTS5kIWpxF0_nMqZO3-Rct7tN8u0witoN5HGi2ha8QUJfEUrad-MOtpOmbkh9bQ54DMlj-m5WzxmB-ZDuzN95AOfbkvHIpZ2j1VLk6ZywQB3deM3PcMfMPTldX4rwMVL-";
//
//			String registrationToken = "ftxZYSaZTAWVuy71ypFmKv:APA91bHF_1ueGexzvUUHhdtDTXb-bmL4gDpYnmyAZvVdQ6nS5uZgWwhFy_CY2FXmDg6019K9WlQSN-FYrAoPb7ReHwYLxiXd4ghogU5ZD7Ye1XilZODLcjP1d6HFy_crhwtAToDfZENV";
//
//
//			boolean perAndroid = false; // scelgo false per IOS
//			
//			Message message = null;
//			
//			if (perAndroid ) {
//			
//			/* ANDROID */
//			 message = Message.builder()
//					.putData("id", String.valueOf(System.currentTimeMillis()))
//				    .putData("title", "TEST TITOLO 4 ")
//				    .putData("sender", "TEST MITTENTE 4 ")
//				    .putData("msg", "Messaggio da visualizzare 4")
//				    .putData("date", String.valueOf(System.currentTimeMillis()))
//				    .setToken(registrationToken)
//				    .build();
//			
//			}
//			else {
//			/*IOS */
//			
//				
//			String sTitolo = "Titolo";
//			String sMsg = "messaggio ripetitivo";
//			
//			JSONObject jsonNotification = new JSONObject();
//			jsonNotification.put("id", 100151); //String.valueOf(System.currentTimeMillis() ));
//			jsonNotification.put("title",  sTitolo );
//			jsonNotification.put("sender", "test mittente 4" );
//			jsonNotification.put("date", ""+String.valueOf(System.currentTimeMillis() ));
//			jsonNotification.put("msg", sMsg);
//
//		
//			
//			
//			//CREO IL MESSAGGIO IOS
//			message = Message.builder()
//					
//					.setToken( registrationToken ) 					        					        					       					        					        					  					    
//					.setApnsConfig(ApnsConfig.builder()
//							.putHeader("apns-priority", "10")
//							.putHeader("apns_push_type", "background")
//							.setAps(Aps.builder()
//									.setSound(CriticalSound.builder() // All fields
//											.setCritical(true)
//											.setName("default")
//											.setVolume(1)
//											.build()) 	
//									.setContentAvailable(true)
//									.putCustomData("eshbody", jsonNotification.toString() )
//									.setContentAvailable(true)																	
//									.setAlert(ApsAlert.builder()
//											.setTitle(sTitolo )
//											.setBody( sMsg )
//											.build())
////									.setBadge(42)
//									.build())
//							.build())
//					.build();
//			}
//			
//			String response = FirebaseMessaging.getInstance().send(message);
//			
//			System.out.println("Successfully sent message: " + response);
//			
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
//	}


	private static void inviaNotificaYouMessage() {		
		try {
			//YOUWORKFORCE CLIENT UNICO
			InputStream serviceAccount = TestFCM.class.getClassLoader().getResourceAsStream("serviceAccount/youworkforce-firebase-adminsdk-72smp-29f94feb0e.json");
			String payload = ResourceUtils.getResourceFileAsString("test-payloads/youmessage.json");

			// YOUMESSAGE NATIVO
			// FileInputStream serviceAccount = new FileInputStream("serviceAccount/youMessageNativo-adminsdk.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://esh-care.firebaseio.com")
			  .build();

			FirebaseApp.initializeApp(options);
			long lDurataMassimaVitaDelMessaggio = 0;
			JSONObject jsDati = JSONObject.fromObject(payload);
			jsDati.put("msg_id", Long.toString(  new Date ().getTime() ));

			//iphone7 youMessage native client
			//String sRegistrationId = "dwLvempCM02xvtRaXkwv-2:APA91bGsXP2TM_XGcTCvC09kbi7jiXAu1OImK1KHVKR09xTwbRZHI4RqmlDgh8itTTZEJu9Ybz-8SHJAr5IQqlgcnOyBcZkFB3q587WpPov8754JGyWkMDbQCY0jSP2eDUa_yvo-eXUJ" ;
			
			//iphone7
			//String sRegistrationId = "fycbQYVMXk-auieilQKq7e:APA91bETUBO89EPlU00M7P3uLyo6BgP2CQcWN-1H3bBvPPZkXf5XY7LN6vQhpiEhicUGZAxmU8SNOWG7vVs-cocsXFEVi8bZKo-hrbKPq4ebaciBYrLeACd1ib5wTvYIdVUIfFdTGswa";
			
			//iphone6
		 	//String sRegistrationId = "eMe8TwNIw0jBvAHepbf9l-:APA91bEH0Vnmmd-YxbH9zQJgtcBl-DxP7XSKxpwuKDhEpPIJ8etsWHdHR1KqIz8fzJL4R6g0mmRQKzr_9EpXKNNcx7nxwclIvyZoNd596hHeIRFtk0iDCyQmZOs-igWaVZxPtQJYDsEM";

			//Davide Android
			String sRegistrationId = "eaC_hbuJR_uhHIbtC4CGhS:APA91bHaDAIxIljZSqenqPUTf8tcfdZaJcEuRh0IE_xTCBYh8Ia-MXVOCOiwubwmORPoiGckwOa5tJoh6ThXqLBuDAXhsqJsCd2A-wlmt8-r7RmE0inQ9G0UE0iCXlNd9-j3zqXX1aea" ;
			System.out.println("****************************************************************************************");
			System.out.println(" DATI: " + jsDati);
			
			if(sRegistrationId != null && sRegistrationId.length() > 0){
				Map<String, String> map = new HashMap<>();
				Set<String> keySet = jsDati.keySet();
				String title = "YouMessage";
				String body = "Nuovo messaggio";

				for(String key: keySet) {

					String val = jsDati.get(key).toString();
					if (key.equalsIgnoreCase("msg_testo")) body = val;
					if (key.equalsIgnoreCase("msg_mittente")) title = val;
					map.put(key, val);
				}
				
				map.put("click_action" , "FLUTTER_NOTIFICATION_CLICK" ) ;
				
				Message message = Message.builder()
						.putAllData(map)
						.setNotification(new Notification(title, body))
					    .setToken(sRegistrationId)
					    .setAndroidConfig(AndroidConfig.builder()
					    		.setTtl(lDurataMassimaVitaDelMessaggio*1000)
					    		.build())
					    .setApnsConfig(ApnsConfig.builder()
								.putHeader("apns-priority", "10")
								.putHeader("apns_push_type", "background")
								.setAps(Aps.builder()
										.setSound(CriticalSound.builder()
												.setCritical(true)
												.setName("default")
												.setVolume(1)
												.build())
										
										//=== YOUMESSAGE NATIVE 
										//without this didReceiveRemote DOESNT FIRE!
										  .setAlert(ApsAlert.builder()
								                    .setTitle("YouMessage")
								                    .setBody("Nuovo messaggio di testo")
								                    .build())
										  //=====
										  
										.putCustomData("notificationbody", jsDati.toString())
										.setContentAvailable(true)																	
										//.setBadge(78)
										.build())
								.build())
						.build();
				
				String response = FirebaseMessaging.getInstance().send(message);
				
				System.out.println("Successfully sent message: " + response);
				
			}
			
		} catch (Exception e) {
			System.out.println("ERRORE: ThreadInvioNotificheYouMessage.inviaNotifica");		
			e.printStackTrace();			
		}
	}

	private static void inviaNotificaYouTalkie() {
		try {
			InputStream serviceAccount = TestFCM.class.getClassLoader().getResourceAsStream("serviceAccount/youworkforce-firebase-adminsdk-72smp-29f94feb0e.json");
			String payload = ResourceUtils.getResourceFileAsString("test-payloads/youtalkie.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://esh-care.firebaseio.com")
					.build();

			FirebaseApp.initializeApp(options);
			long lDurataMassimaVitaDelMessaggio = 0;
			JSONObject jsDati = JSONObject.fromObject(payload);
			jsDati.put("msg_id", Long.toString(  new Date ().getTime() ));

			//Davide
			String sRegistrationId = "eaC_hbuJR_uhHIbtC4CGhS:APA91bHaDAIxIljZSqenqPUTf8tcfdZaJcEuRh0IE_xTCBYh8Ia-MXVOCOiwubwmORPoiGckwOa5tJoh6ThXqLBuDAXhsqJsCd2A-wlmt8-r7RmE0inQ9G0UE0iCXlNd9-j3zqXX1aea" ;
			System.out.println("****************************************************************************************");
			System.out.println(" DATI: " + jsDati);

			if(sRegistrationId != null && sRegistrationId.length() > 0){
				Map<String, String> map = new HashMap<>();
				Set<String> keySet = jsDati.keySet();
				String title = "";
				String body = "";

				for(String key: keySet) {

					String val = jsDati.get(key).toString();
					if (key.equalsIgnoreCase("msg_testo")) body = val;
					if (key.equalsIgnoreCase("msg_mittente")) title = val;
					map.put(key, val);
				}

				map.put("click_action" , "FLUTTER_NOTIFICATION_CLICK" ) ;



				Message message = Message.builder()
						.putAllData(map)
						.setNotification(new Notification(title, body))
						.setToken(sRegistrationId)
						.setAndroidConfig(AndroidConfig.builder()
								.setTtl(lDurataMassimaVitaDelMessaggio*1000)
								.build())
						.setApnsConfig(ApnsConfig.builder()
								.putHeader("apns-priority", "10")
								.putHeader("apns_push_type", "background")
								.setAps(Aps.builder()
										.setSound(CriticalSound.builder()
												.setCritical(true)
												.setName("ricezione.caf")
												.setVolume(1)
												.build())

										//=== YOUMESSAGE NATIVE
										//without this didReceiveRemote DOESNT FIRE!
										.setAlert(ApsAlert.builder()
												.setTitle("You Talkie")
												.setBody("Messaggio di test")
												.build())
										//=====

										.putCustomData("notificationbody", jsDati.toString())
										.setContentAvailable(true)
										//.setBadge(78)
										.build())
								.build())
						.build();

				String response = FirebaseMessaging.getInstance().send(message);

				System.out.println("Successfully sent message: " + response);

			}

		} catch (Exception e) {
			System.out.println("ERRORE: ThreadInvioNotificheYouMessage.inviaNotifica");
			e.printStackTrace();
		}
	}

	private static void inviaNotificaConfermaYouMessage() {		
		try {
			
			FileInputStream serviceAccount = new FileInputStream("serviceAccount/youworkforce-firebase-adminsdk-72smp-29f94feb0e.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://esh-care.firebaseio.com")
			  .build();

			FirebaseApp.initializeApp(options);

			long lDurataMassimaVitaDelMessaggio = 0;
			String sJson = "{\"tipo\":\"MESSAGGIO_TESTO\","
					+ "\"data_invio\":\"1594890736010\","
					+ "\"msg_id\":\"9663\","
					+ "\"msg_tipo\":\"std\","
					+ "\"msg_conversazione\":\"vec.restyle\","
					+ "\"msg_mittente\":\"vec.restyle\","
					+ "\"msg_testo\":\"Prova da Andrea. Ignorare\","
					+ "\"msg_dataora\":\"1594890735000\","
					+ "\"msg_bloccante\":\"0\","
					+ "\"msg_tp_int\":\"\","
					+ "\"msg_option\":\"\","
					+ "\"msg_tipodestinatario\":\"U\","
					+ "\"msg_destinatario\":\"vec.011\","
					+ "\"msg_iddestinatario\":\"11\","
					+ "\"msg_allegati\":\"\"}";
			
			JSONObject jsDati = JSONObject.fromObject(sJson);
			String sRegistrationId = "cotDD3rZfEXuh73oEs00t0:APA91bFNlq86jslLOW4mTs42UddPtaZ-xSAdFJekTwBMc7GdRu5xiT_9jBXGlWu3FqKV7y8epORi7I4KlRHBTi-P2yh9sb_C66AY5feFK2-SArJxNzKscJVPRLJT2yAriAKFZQydmP19";
			System.out.println("****************************************************************************************");
			System.out.println(" DATI: " + jsDati);
			
			if(sRegistrationId != null && sRegistrationId.length() > 0){
				Map<String, String> map = new HashMap<>();
				Set<String> keySet = jsDati.keySet();
				String title = "";
				String body = "";

				for(String key: keySet) {

					String val = jsDati.get(key).toString();
					if (key.equalsIgnoreCase("msg_testo")) body = val;
					if (key.equalsIgnoreCase("msg_mittente")) title = val;
					map.put(key, val);
				}
				
				
				
				Message message = Message.builder()
						.putAllData(map)
						.setNotification(new Notification(title, body))
					    .setToken(sRegistrationId)
					    .setAndroidConfig(AndroidConfig.builder()
					    		.setTtl(lDurataMassimaVitaDelMessaggio*1000)
					    		.build())
					    .setApnsConfig(ApnsConfig.builder()
								.putHeader("apns-priority", "10")
								.putHeader("apns_push_type", "background")
								.setAps(Aps.builder()
										.putCustomData("notificationbody", jsDati.toString())
										.setContentAvailable(true)																	
										.setBadge(78)
										.build())
								.build())
						.build();
				
				String response = FirebaseMessaging.getInstance().send(message);
				
				System.out.println("Successfully sent message: " + response);
				
			}
			
		} catch (Exception e) {
			System.out.println("ERRORE: ThreadInvioNotificheYouMessage.inviaNotifica");		
			e.printStackTrace();			
		}
	}	
	
	private static void inviaComandoYouMessage(String comando) {		
		try {
			
			FileInputStream serviceAccount = new FileInputStream("serviceAccount/youworkforce-firebase-adminsdk-72smp-29f94feb0e.json");

			FirebaseOptions options = new FirebaseOptions.Builder()
			  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
			  .setDatabaseUrl("https://esh-care.firebaseio.com")
			  .build();

			FirebaseApp.initializeApp(options);

			long lDurataMassimaVitaDelMessaggio = 0;
			String sJson = "{\"tipo\": "
					+ "\""
					+ comando
					+ "\""
					+ "}";
			
			JSONObject jsDati = JSONObject.fromObject(sJson);
			//iphone7
			String sRegistrationId = "fycbQYVMXk-auieilQKq7e:APA91bETUBO89EPlU00M7P3uLyo6BgP2CQcWN-1H3bBvPPZkXf5XY7LN6vQhpiEhicUGZAxmU8SNOWG7vVs-cocsXFEVi8bZKo-hrbKPq4ebaciBYrLeACd1ib5wTvYIdVUIfFdTGswa";
			
			
			//iphone6
		     //String sRegistrationId = "eMe8TwNIw0jBvAHepbf9l-:APA91bEH0Vnmmd-YxbH9zQJgtcBl-DxP7XSKxpwuKDhEpPIJ8etsWHdHR1KqIz8fzJL4R6g0mmRQKzr_9EpXKNNcx7nxwclIvyZoNd596hHeIRFtk0iDCyQmZOs-igWaVZxPtQJYDsEM";

			//Davide
			//String sRegistrationId = "ea2b1EZ1oUNbtWR3DsKSJV:APA91bHBIdpAWyzWbLpkn8NBiG84mtYbnVnw99MetspXcrCr-LlCJ_AlAQHsqPL4JXOdKoCnfMFJtXwIm0ZmhY-edUbDoacadbKb-BN09ERPk9wYaHKqWxaQh5ABjskHNe-Exuney27q" ;
			
			System.out.println("****************************************************************************************");
			System.out.println(" DATI: " + jsDati);
			
			if(sRegistrationId != null && sRegistrationId.length() > 0){
				Map<String, String> map = new HashMap<>();
				Set<String> keySet = jsDati.keySet();
				String title = "";
				String body = "";

				for(String key: keySet) {

					String val = jsDati.get(key).toString();
					if (key.equalsIgnoreCase("msg_testo")) body = val;
					if (key.equalsIgnoreCase("msg_mittente")) title = val;
					map.put(key, val);
				}
				
				
				
				Message message = Message.builder()
						.putAllData(map)
						.setNotification(new Notification(title, body))
					    .setToken(sRegistrationId)
					    .setAndroidConfig(AndroidConfig.builder()
					    		.setTtl(lDurataMassimaVitaDelMessaggio*1000)
					    		.build())
					    .setApnsConfig(ApnsConfig.builder()
								.putHeader("apns-priority", "10")
								.putHeader("apns_push_type", "background")
								.setAps(Aps.builder()
										.putCustomData("notificationbody", jsDati.toString())
										.setContentAvailable(true)																	
										//.setBadge(78)
										.build())
								.build())
						.build();
				
				String response = FirebaseMessaging.getInstance().send(message);
				
				System.out.println("Successfully sent message: " + response);
				
			}
			
		} catch (Exception e) {
			System.out.println("ERRORE: ThreadInvioNotificheYouMessage.inviaNotifica");		
			e.printStackTrace();			
		}
	}


}